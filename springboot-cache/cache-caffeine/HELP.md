# 基于SpringBoot 使用 Caffeine 本地缓存

目录
- 本地缓存介绍
- 缓存组件Caffeine介绍
- SpringBoot集成Caffeine两种方式

### 一、本地缓存介绍
Caffeine是基于JAVA 1.8 Version的高性能缓存库。Caffeine提供的内存缓存使用参考Google guava的API。Caffeine是基于Google Guava Cache设计经验上改进的成果,在Spring Boot 2.0中将取代，基于LRU算法实现，支持多种缓存过期策略。



> 缓存在日常开发中启动至关重要的作用，由于是存储在内存中，数据的读取速度是非常快的，能大量减少对数据库的访问，减少数据库的压力。
### 二、缓存组件Caffeine介绍
Caffeine 是基于 JAVA 8 的高性能缓存库。并且在 spring5 (springboot 2.x) 后，spring 官方放弃了 Guava，而使用了性能更优秀的 Caffeine 作为默认缓存组件。

1.Caffeine性能

2.Caffeine配置说明

|参数|类型|描述|
|----|---|----|
|initialCapacity|integer|初始的缓存空间大小|
|maximumSize|long|设置缓存最大条目数，超过条目则触发回收|
|maximumWeight|long|设置缓存最大权重，设置权重是通过weigher方法， 需要注意的是权重也是限制缓存大小的参数，并不会影响缓存淘汰策略|
|expireAfterWrite|duration|写入后隔段时间过期|
|expireAfterAccess|duration|访问后隔断时间过期|
|refreshAfterWrite|duration|写入后隔断时间刷新|
|removalListener|duration|缓存淘汰监听器，配置监听器后，每个条目淘汰时都会调用该监听器|
|writer|--|writer监听器其实提供了两个监听，一个是缓存写入或更新是的write，一个是缓存淘汰时的delete，每个条目淘汰时都会调用该监听器|
|weakKeys|boolean|将key设置为弱引用，在GC时可以直接淘汰|
|weakValues|boolean|将value设置为弱引用，在GC时可以直接淘汰|
|softValues|boolean|将value设置为软引用，在内存溢出前可以直接淘汰|
|recordStats|--|开发统计功能|

> 注意
> - weakValues和softValues不可以同时使用；
> - maximumSize和maximumWeight不可以同时使用；
> - expireAfterWrite和expireAfterAccess同事存在时，以expireAfterWrite为准。

3.软引用与弱引用

软引用：如果一个对象只具有软引用，则内存空间足够，垃圾回收器就不会回收它；如果内存空间不足了，就会回收这些对象的内存。

    Caffeine.newBuilder().softValues().build(); // 软引用

弱引用：弱引用的对象拥有更短暂的生命周期。在垃圾回收器线程扫描它所管辖的内存区域的过程中，一旦发现了只具有弱引用的对象，不管当前内存空间足够与否，都会回收它的内存

    Caffeine.newBuilder().weakKeys().weakValues().build(); // 弱引用
    
 ### 三、SpringBoot集成Caffeine两种方式
 
 SpringBoot 有俩种使用 Caffeine 作为缓存的方式：
 
 - 方式一：直接引入 Caffeine 依赖，然后使用 Caffeine 方法实现缓存。
 
 - 方式二：引入 Caffeine 和 Spring Cache 依赖，使用 SpringCache 注解方法实现缓存。